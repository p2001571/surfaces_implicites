#version 330

#ifdef VERTEX_SHADER
layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normals;

uniform mat4 vpMatrix;
out vec3 n;

void main()
{
    n = normals;
    gl_Position = vpMatrix * vec4(position, 1);
}

#endif


#ifdef FRAGMENT_SHADER
out vec4 fragment_color;
vec3 light = normalize(vec3(0.2, 0.2, -0.8));
in vec3 n;
uniform vec3 color;

void main()
{
    float cos_theta = dot(light, n);
    fragment_color = vec4(color * cos_theta, 1);
}

#endif
