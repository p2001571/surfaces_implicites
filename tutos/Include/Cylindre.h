#pragma once

#pragma once
#include "./tutos/Include/implicits.h"

struct Cylindre : public AnalyticScalarField
{
    Vector3 pos;
    float r;

    Cylindre(const Vector3& pos1, float r1) : pos(pos1), r(r1) {}

    double Value(const Vector3& p) const
    {
        return sqrt(pow(p[0] - pos[0], 2) + pow(p[2] - pos[2], 2)) - r;
    }
};