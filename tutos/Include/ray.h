#ifndef __Ray__
#define __Ray__

#include "mathematics.h"

// Ray class
class Ray
{
protected:
  Vector3 c; //!< Origin of the ray.
  Vector3 n; //!< Direction.
public:
  //! Empty.
  Ray() {}
  explicit Ray(const Vector3&, const Vector3&);
  //! Empty.
  ~Ray() {}

  Ray Reflect(const Vector3&, const Vector3&);

  Vector3 operator()(double) const;

  // Functions to access Vector3 class components
  Vector3 Origin() const;
  Vector3 Direction() const;

  friend std::ostream& operator<<(std::ostream&, const Ray&);
};

/*!
\brief Creates a ray.

The direction should be unit:
\code
Ray ray(Vector3(0.0,0.0,0.0),Normalized(Vector3(2.0,-1.0,3.0)));
\endcode
\param p Origin.
\param d Direction (should be unit vector).
*/
inline Ray::Ray(const Vector3& p, const Vector3& d)
{
  c = p;
  n = d;
}

/*!
\brief Return the origin of the ray.
*/
inline Vector3 Ray::Origin() const
{
  return c;
}

/*!
\brief Return the direction of the ray.
*/
inline Vector3 Ray::Direction() const
{
  return n;
}

/*!
\brief Computes the location of a vertex along the ray.
\param t Parameter.
*/
inline Vector3 Ray::operator()(double t) const
{
  return c + t * n;
}

#endif
