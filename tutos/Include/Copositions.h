#pragma once
#include "./tutos/Include/implicits.h"

class Union : public AnalyticScalarField
{
private:
	const AnalyticScalarField* a1;
	const AnalyticScalarField* a2;

public:
	Union(const AnalyticScalarField& ana1, const AnalyticScalarField& ana2)
	{
		a1 = &ana1;
		a2 = &ana2;
	}

	double Value(const Vector3& p) const
	{
		return std::min(a1->Value(p), a2->Value(p));
	}

	bool Intersect(const Ray& r, double& t) const
	{
		double t1 = 0;
		double t2 = 0;
		bool inter1 = a1->Intersect(r, t1);
		bool inter2 = a2->Intersect(r, t2);

		if (t1 != 0)
			t = t1;
			
		if (t2 != 0)
			t = std::min(t, t2);
		
		if (inter1 || inter2)
			return true;

		return false;
	}

	void freeMemory() const
	{
		a1->freeMemory();
		a2->freeMemory();
	}
};

class Intersection : public AnalyticScalarField
{
private:
	const AnalyticScalarField* a1;
	const AnalyticScalarField* a2;

public:
	Intersection(const AnalyticScalarField& ana1, const AnalyticScalarField& ana2)
	{
		a1 = &ana1;
		a2 = &ana2;
	}

	double Value(const Vector3& p) const
	{
		return std::max(a1->Value(p), a2->Value(p));
	}
};

class Difference : public AnalyticScalarField
{
private:
	const AnalyticScalarField* a1;
	const AnalyticScalarField* a2;

public:
	Difference(const AnalyticScalarField& ana1, const AnalyticScalarField& ana2)
	{
		a1 = &ana1;
		a2 = &ana2;
	}

	double Value(const Vector3& p) const
	{
		return std::max(a1->Value(p), -a2->Value(p));
	}

	bool Intersect(const Ray& r, double& t) const
	{
		double t1 = 0;
		double t2 = 0;
		bool inter1 = a1->Intersect(r, t1);
		bool inter2 = a2->Intersect(r, t2);


		if (inter1 && inter2)
		{
			t = t1 + (t1 - t2);
			return true;
		}
		else if (inter1)
		{
			t = t1;
			return true;
		}

		return false;
	}

	void freeMemory() const
	{
		a1->freeMemory();
		a2->freeMemory();
	}
};