#pragma once
#include <corecrt_math.h>
#include <vec.h>

enum composition { _union, _intersection, _difference };

struct Node
{
    Node() {}

    virtual double Value(const Vector& p) const = 0;
};


struct Union : public Node
{
    const Node* node1;
    const Node* node2;

    Union(const Node& n1, const Node& n2)
    {
        this->node1 = &n1;
        this->node2 = &n2;
    }

    double Value(const Vector& p) const
    {
        return std::min(node1->Value(p), node2->Value(p));
    }
};

