// Vector3  

// Self include
#include "../Include/mathematics.h"

#include <iostream>

/*!
\class Vector3 mathematics.h
\brief Vector3s in three dimensions.

Most binary operators have been overloaded as expected,
destructive operators, such as addition and subtraction
have been implemented and behave as one could expect.

<P><I>How do I compute the cross product of two vectors?</I>
<BR>Simply use the overloaded Vector3::operator/, for instance
\code
Vector3 c=a/b; // Cross product
\endcode
computes the cross product of a and b.
<P><I>How can I get access to the x, y and z components of a vector?</I>
<BR>Use v[0], v[1] and v[2] to get access to the x, y and z components of a vector v respectively.
<P><I>How do I compute the normal of a triangle?</I>
<BR>Let a,b,c the vertices of the triangle, simply compute the cross product
\code
Vector3 n=(a-b)/(a-c);  // Cross product
\endcode
or use the member function of the Triangle class:
\code
Vector3 n=Triangle(a,b,c).Normal(); // Compute the normal
\endcode
<P><I>How can I sort the three elements in a vector?</I>
<BR>Use Vector3::Sort() as follows:
\code
Vector3 s=Vector3(2.0,3.0,-1.0).Sort(); // Sort components in ascending order
\endcode

<P><I>How do I perform bi-linear interpolation on vectors?</I>
<BR>Use Vector3::Bilinear() with four vectors and bilinear coordinates.
Alternatively, some geometric classes implement bilinear interpolation,
such as Quadrangle::Vertex().
*/

const Vector3 Vector3::Null = Vector3(0.0, 0.0, 0.0);
const Vector3 Vector3::X = Vector3(1.0, 0.0, 0.0);
const Vector3 Vector3::Y = Vector3(0.0, 1.0, 0.0);
const Vector3 Vector3::Z = Vector3(0.0, 0.0, 1.0);

/*!
\brief Normalize a vector, computing the inverse of its norm and scaling
the components.

This function does not check if the vector is null,
which might resulting in errors.
*/
void Normalize(Vector3& u)
{
  u *= 1.0 / Norm(u);
}

/*!
\brief Returns a vector orthogonal to the argument vector.

The returned orthogonal vector is not computed randomly.
First, we find the two coordinates of the argument vector with
maximum absolute value. The orthogonal vector is defined by
swapping those two coordinates and changing one sign, whereas
the third coordinate is set to 0.

The returned orthogonal vector lies in the plane orthogonal
to the first vector.
*/
Vector3 Vector3::Orthogonal() const
{
  Vector3 a = Abs(*this);
  int i = 0;
  int j = 1;
  if (a[0] > a[1])
  {
    if (a[2] > a[1])
    {
      j = 2;
    }
  }
  else
  {
    i = 1;
    j = 2;
    if (a[0] > a[2])
    {
      j = 0;
    }
  }
  a = Vector3::Null;
  a[i] = c[j];
  a[j] = -c[i];
  return a;
}

/*!
\brief Overloaded output-stream operator.
\param u Vector3.
\param s Stream.
*/
std::ostream& operator<<(std::ostream& s, const Vector3& u)
{
  s << "Vector3(" << u.c[0] << ',' << u.c[1] << ',' << u.c[2] << ')';
  return s;
}

/*!
\brief Given a vector, creates two vectors xand y that form an orthogonal basis.

This algorithm pickes the minor axis in order to reduce numerical instability
\param x, y Returned vectors such that (x,y,n) form an orthonormal basis (provided n is normalized).
*/
void Vector3::Orthonormal(Vector3& x, Vector3& y) const
{
  x = Normalized(Orthogonal());
  y = Normalized(*this / x);
}
