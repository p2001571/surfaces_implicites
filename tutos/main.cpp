#include "Include/implicits.h"
#include "app_camera.h"
#include <uniforms.h>
#include "draw.h"
#include "Include/Sphere.h"
#include "Include/Copositions.h"
#include "Include/Boite.h"
#include "Include/Cylindre.h"
#include "Include/Ellipsoid.h"
#include <chrono>


std::vector<Sphere> castSphere(const AnalyticScalarField& obj, int nb_rays, float sphere_radius)
{
    std::vector<Sphere> spheres;

    for (int i = 0; i < nb_rays; i++)
    {
        float x = -0.5 + ((float)(rand() % 11) / 20) * 2;
        float y = -0.5 + ((float)(rand() % 11) / 20) * 2;

        Ray r(Vector3(0, 0, -10), Vector3(x, y, 1));
        double t = 0;

        if (obj.Intersect(r, t))
            spheres.push_back(Sphere(r(t), sphere_radius));
    }
    return spheres;
}

AnalyticScalarField* createObjMinusSphereUnion(AnalyticScalarField& obj, std::vector<Sphere> spheres)
{
    AnalyticScalarField* new_obj = nullptr;

    if (spheres.size() == 1)
        new_obj = new Sphere(spheres[0]);

    else if (spheres.size() >= 2)
    {
        new_obj = new Union(*new Sphere(spheres[0]), *new Sphere(spheres[1]));

        for (int i = 2; i < spheres.size(); i++)
            new_obj = new Union(*new_obj, *new Sphere(spheres[i]));
    }

    if (new_obj != nullptr)
        new_obj = new Difference(obj, *new_obj);
    else
        new_obj = &obj;

    return new_obj;
}


class TP : public AppCamera
{
public:

    TP() : AppCamera(1024, 640) {}

    void callsStat()
    {
        int nb_call = 10000000;

        std::string objects_name[4] = { "Sphere", "Cylinder", "Boite", "Ellipsoid" };

        AnalyticScalarField* objs[4] = {
            new Sphere(Vector3(0, 0, 0), 1),
            new Cylindre(Vector3(0, 0, 0), 1),
            new Boite(Vector3(0, 0, 0), Vector3(1, 1, 1)),
            new Ellipsoid(Vector3(0, 0, 0), Vector3(1, 2, 1))
        };

        for (int k = 0; k < 4; k++)
        {
            std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();

            for (int i = 0; i < nb_call; i++)
                objs[k]->Value(Vector3(0, 0, 0));

            std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();
            unsigned int time = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
            std::cout << "Time for " << objects_name[k] << " = " << time << "ms" << std::endl;

            objs[k]->freeMemory();
        }

    }

    int init()
    {
        callsStat();

        initArbreConstruction();
        //initVieillissement();
        initBuffers();

        return 0;
    }

    void initArbreConstruction()
    {
        Union obj(
            Intersection(
                Ellipsoid(Vector3(2, 10.39, 0), Vector3(1, 0.5, 1)),
                Boite(Vector3(2, 10.39, 0), Vector3(1, 1, 1))
            ),
            Union(
                Union(
                    Intersection(
                        Cylindre(Vector3(0, 0, 0), 0.8),
                        Boite(Vector3(0, 11, 0), Vector3(2, 1, 2))),
                    Ellipsoid(Vector3(0, 10.4, 0), Vector3(1.5, 1, 1.5))),
                Union(
                    Union(
                        Intersection(
                            Boite(Vector3(2, 8, 0), Vector3(2, 0.5, 2)),
                            Difference(
                                Ellipsoid(Vector3(2, 6.5, 0), Vector3(0.8, 2, 0.8)),
                                Ellipsoid(Vector3(2, 5, 0), Vector3(0.8, 2, 0.8)))),
                        Intersection(
                            Cylindre(Vector3(2, 0, 0), 1),
                            Boite(Vector3(2, 9.2, 0), Vector3(2, 1.2, 2)))),
                    Union(
                        Difference(
                            Ellipsoid(Vector3(0, 6, 0), Vector3(2.1, 0.5, 2.1)),
                            Ellipsoid(Vector3(0, -3, 0), Vector3(2.8, 10, 2.8))),
                        Union(
                            Union(
                                Intersection(
                                    Cylindre(Vector3(0, 0, 0), 1.5),
                                    Boite(Vector3(0, 9.2, 0), Vector3(2, 1.2, 2))),
                                Intersection(
                                    Ellipsoid(Vector3(0, 8, 0), Vector3(1.5, 0.8, 1.5)),
                                    Boite(Vector3(0, 8, 0), Vector3(2, 1, 2)))),
                            Difference(
                                Difference(
                                    Ellipsoid(Vector3(0, -2, 0), Vector3(3, 10, 3)),
                                    Ellipsoid(Vector3(0, -3, 0), Vector3(2.8, 10, 2.8))),
                                Boite(Vector3(0, -5, 0), Vector3(10, 7, 10))))))));

       
        Box b(Vector3(-3, -1, -3), Vector3(3, 13, 3));
        obj.Polygonize(200, m, b);

        camera().lookat(Point(-10, -10, -10), Point(10, 10, 10));
    }

    void initVieillissement()
    {
        camera().lookat(Point(-10, -10, -10), Point(10, 10, 10));
        Box b(Vector3(-5, -5, -5), Vector3(5, 5, 5));

        Sphere* obj = new Sphere(Vector3(0, 0, 0), 3);

        std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();

        std::vector<Sphere> big_spheres = castSphere(*obj, 20, 0.8);
        AnalyticScalarField* new_obj = createObjMinusSphereUnion(*obj, big_spheres);

        std::vector<Sphere> medium_spheres = castSphere(*new_obj, 50, 0.5);
        new_obj = createObjMinusSphereUnion(*new_obj, medium_spheres);

        std::vector<Sphere> small_spheres = castSphere(*new_obj, 100, 0.3);
        new_obj = createObjMinusSphereUnion(*new_obj, small_spheres);

        std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();
        unsigned int time = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
        std::cout << "Time for sphere union: " << time << "ms" << std::endl;

        printf("Nb spheres: %d\n", big_spheres.size() + medium_spheres.size() + small_spheres.size());

        new_obj->Polygonize(300, m, b);
        new_obj->freeMemory();
    }

    void initBuffers()
    {
        std::vector<Vector3> m_vertices = m.getVertices();
        std::vector<Vector3> m_normals = m.getNormals();

        for (unsigned int& i : m.VertexIndexes())
        {
            vertices.push_back(vec3(m_vertices[i][0], m_vertices[i][1], m_vertices[i][2]));
            normals.push_back(vec3(m_normals[i][0], m_normals[i][1], m_normals[i][2]));
        }

        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);

        // vertices
        glGenBuffers(1, &vbo_vertices);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_vertices);

        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(vec3), &vertices.front(), GL_STATIC_DRAW);

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(0);

        // normals
        glGenBuffers(1, &vbo_normals);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_normals);

        glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(vec3), &normals.front(), GL_STATIC_DRAW);

        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(1);

        shader_program = read_program("tutos/shader.glsl");
        program_print_errors(shader_program);

        glClearDepth(1.f);
        glDepthFunc(GL_LESS);
        glEnable(GL_DEPTH_TEST);
    }

    int quit()
    {
        glDeleteVertexArrays(1, &vao);
        glDeleteBuffers(1, &vbo_vertices);
        release_program(shader_program);
        return 0;
    }


    int render()
    {
        glClearColor(0.36, 0.55, 1.0f, 1.f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        Transform view = camera().view();
        Transform projection = camera().projection();
        Transform vp = projection * view;

        glUseProgram(shader_program);
        glBindVertexArray(vao);

        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        program_uniform(shader_program, "color", vec3(1, 1, 1));
        program_uniform(shader_program, "vpMatrix", vp);
        glDrawArrays(GL_TRIANGLES, 0, vertices.size());

        return 1;
    }

private:
    GLuint vao;
    GLuint vbo_vertices;
    GLuint vbo_normals;
    GLuint shader_program;

    Mesh2 m;
    std::vector<vec3> vertices;
    std::vector<vec3> normals;
};



int main(int argc, char** argv)
{
    srand(time(NULL));
    TP tp;
    tp.run();

    return 0;
}