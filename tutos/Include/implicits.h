// Implicits

#pragma once

#include <iostream>
#include "../Include/mesh2.h"



class AnalyticScalarField
{
protected:
public:
  AnalyticScalarField();
  virtual double Value(const Vector3&) const;
  virtual Vector3 Gradient(const Vector3&) const;

  // Normal
  virtual Vector3 Normal(const Vector3&) const;

  // Dichotomy
  Vector3 Dichotomy(Vector3, Vector3, double, double, double, const double& = 1.0e-4) const;

  virtual void Polygonize(int, Mesh2&, const Box&, const double& = 1e-4) const;

  virtual bool Intersect(const Ray&, double&) const;

  virtual void freeMemory() const;
protected:
  static const double Epsilon; //!< Epsilon value for partial derivatives
protected:
  static int TriangleTable[256][16]; //!< Two dimensionnal array storing the straddling edges for every marching cubes configuration.
  static int edgeTable[256];    //!< Array storing straddling edges for every marching cubes configuration.
};