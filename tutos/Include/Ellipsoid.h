#pragma once
#include "./tutos/Include/implicits.h"

struct Ellipsoid : public AnalyticScalarField
{
    Vector3 pos;
    Vector3 size;

    Ellipsoid(const Vector3& pos1, const Vector3& size1) : pos(pos1), size(size1) {}

    double Value(const Vector3& p) const
    {
        double res = -1;

        for (int i = 0; i < 3; i++)
            res += pow(p[i] - pos[i], 2) / (size[i]*size[i]);

        return res;
    }
};