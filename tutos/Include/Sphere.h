#pragma once
#include "./tutos/Include/implicits.h"

struct Sphere : public AnalyticScalarField
{
    Vector3 c;
    double r;

    Sphere (Vector3 center, double radius)
    {
        c = center;
        r = radius;
    }

    Sphere(const Sphere& s)
    {
        c = s.c;
        r = s.r;
    }

    double Value(const Vector3& p) const
    {
        return Norm(p - c) - r;
    }

    Vector3 Normal(const Vector3& p) const
    {
        Vector3 n = p - c;
        Normalize(n);
        return n;
    }

    Sphere& operator = (const Sphere& s)
    {
        c = s.c;
        r = s.r;
    }

    bool Intersect(const Ray& r, double& t) const
    {
        double last_dist = FLT_MAX;

        while (t < FLT_MAX)
        {
            double d = Value(r(t));

            if (d <= 0)
                return true;
            else
                t += std::max(d, 0.1);

            if (d > last_dist)
                return false;
            else
                last_dist = d;
        }
        return false;
    }

    void freeMemory() const 
    {
        delete this;
    }
};