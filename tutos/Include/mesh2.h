#pragma once

#include "../Include/box.h"
#include "../Include/ray.h"
#include "../Include/mathematics.h"

// Triangle
class Triangle
{
protected:
  Vector3 p[3] = {Vector3(0.0,0.0,0.0),Vector3(1.0,0.0,0.0), Vector3(0.0,1.0,0.0), }; //!< Array of vertices.
public:
  //! Empty.
  Triangle() {}
  explicit Triangle(const Vector3&, const Vector3&, const Vector3&);

  //! Empty.
  ~Triangle() {}

  Vector3 operator[] (int) const;

  // Point in triangle
  Vector3 Vertex(double, double) const;

  // Intersection


  // Geometry
  Vector3 Center() const;

  double Area() const;

  // Stream
  friend std::ostream& operator<<(std::ostream&, const Triangle&);

protected:
  static double epsilon; //!< Internal epsilon constant.
};

/*!
\brief Return the i-th vertex.
\param i Index.
*/
inline Vector3 Triangle::operator[] (int i) const
{
  return p[i];
}

inline Vector3 Triangle::Vertex(double, double) const
{
	return Vector3();
}

//! Compute the barycenter of the triangle.
inline Vector3 Triangle::Center() const
{
  return (p[0] + p[1] + p[2]) / 3.0;
}

//! Compute the area of the triangle.
inline double Triangle::Area() const
{
  return 0.5 * Norm((p[0] - p[1]) / (p[2] - p[0]));
}

/*!
\brief Create a triangle.
\param a,b,c Vertices of the triangle.
*/
inline Triangle::Triangle(const Vector3& a, const Vector3& b, const Vector3& c)
{
  p[0] = a;
  p[1] = b;
  p[2] = c;
}




class Mesh2
{
protected:
  std::vector<Vector3> vertices; //!< Vertices.
  std::vector<Vector3> normals;  //!< Normals.
  std::vector<unsigned int> varray;     //!< Vertex indexes.
  std::vector<unsigned int> narray;     //!< Normal indexes.

public:
  explicit Mesh2();
  explicit Mesh2(const std::vector<Vector3>&, const std::vector<unsigned int>&);
  explicit Mesh2(const std::vector<Vector3>&, const std::vector<Vector3>&, const std::vector<unsigned int>&, const std::vector<unsigned int>&);
  ~Mesh2();

  void Reserve(int, int, int, int);

  Triangle GetTriangle(int) const;
  Vector3 Vertex(int) const;
  Vector3 Vertex(int, int) const;

  Vector3 Normal(int) const;

  int Triangles() const;
  int Vertexes() const;

  std::vector<unsigned int> VertexIndexes() const;
  std::vector<unsigned int> NormalIndexes() const;

  int VertexIndex(int, int) const;
  int NormalIndex(int, int) const;

  Vector3 operator[](int) const;

  Box GetBox() const;

  void Scale(double);

  void SmoothNormals();

  // Constructors from core classes
  explicit Mesh2(const Box&);

  std::vector<Vector3> getVertices() const;
  std::vector<Vector3> getNormals() const;


protected:
  void AddTriangle(int, int, int, int);
  void AddSmoothTriangle(int, int, int, int, int, int);
  void AddSmoothQuadrangle(int, int, int, int, int, int, int, int);
  void AddQuadrangle(int, int, int, int);
};

inline std::vector<Vector3> Mesh2::getVertices() const
{
	return vertices;
}

inline std::vector<Vector3> Mesh2::getNormals() const
{
	return normals;
}

/*!
\brief Return the set of vertex indexes.
*/
inline std::vector<unsigned int> Mesh2::VertexIndexes() const
{
  return varray;
}

/*!
\brief Return the set of normal indexes.
*/
inline std::vector<unsigned int> Mesh2::NormalIndexes() const
{
  return narray;
}

/*!
\brief Get the vertex index of a given triangle.
\param t Triangle index.
\param i Vertex index.
*/
inline int Mesh2::VertexIndex(int t, int i) const
{
  return varray.at(t * 3 + i);
}

/*!
\brief Get the normal index of a given triangle.
\param t Triangle index.
\param i Normal index.
*/
inline int Mesh2::NormalIndex(int t, int i) const
{
  return narray.at(t * 3 + i);
}

/*!
\brief Get a triangle.
\param i Index.
\return The triangle.
*/
inline Triangle Mesh2::GetTriangle(int i) const
{
  return Triangle(vertices.at(varray.at(i * 3 + 0)), vertices.at(varray.at(i * 3 + 1)), vertices.at(varray.at(i * 3 + 2)));
}

/*!
\brief Get a vertex.
\param i The index of the wanted vertex.
\return The wanted vertex (as a 3D Vector3).
*/
inline Vector3 Mesh2::Vertex(int i) const
{
  return vertices[i];
}

/*!
\brief Get a vertex from a specific triangle.
\param t The number of the triangle wich contain the wanted vertex.
\param v The triangle vertex: 0, 1, or 2.
\return The wanted vertex (as a 3D Vector3).
*/
inline Vector3 Mesh2::Vertex(int t, int v) const
{
  return vertices[varray[t * 3 + v]];
}

/*!
\brief Get the number of vertices in the geometry.
\return The number of vertices in the geometry, in other words the size of vertices.
*/
inline int Mesh2::Vertexes() const
{
  return int(vertices.size());
}

/*!
\brief Get a normal.
\param i Index of the wanted normal.
\return The normal.
*/
inline Vector3 Mesh2::Normal(int i) const
{
  return normals[i];
}

/*!
\brief Get the number of triangles.
*/
inline int Mesh2::Triangles() const
{
  return int(varray.size()) / 3;
}

/*!
\brief Get a vertex.
\param i The index of the wanted vertex.
\return The wanted vertex (as a 3D Vector3).
\see vertex(int i) const
*/
inline Vector3 Mesh2::operator[](int i) const
{
  return vertices[i];
}

